Enable opam in current shell : eval $(opam config env)

frama-c cartesian-trees.c -wp -wp-prover script,alt-ergo,Z3,coq -wp-timeout 240 -then-report
