Enable opam in current shell : eval $(opam config env)

frama-c cartesian-trees.c -wp -wp-prover script,alt-ergo,Z3,coq -wp-timeout 240 -then-report

-wp-par 4 :
  s_untouched 32.5s
  left_smallest 7.4s
  left_small 1'10s

# Rapport:
test de neighbour_left
chgt de neighbour_left en neighbour_right
  - chgt de l'algo par symetrie
	[1..x+1]+0 -> [x+1..length]+(length+1)
		length+1 pour conserver les formulations des annotations d'origine utilisant la continuité
	for(x=length-1;x>0;x--) marche pas a cause de size_t
	for(y=0;y<length-1;y++){ x=length-1-y; ...}
  - test de l'algo
  - chgt des annotations par symétrie
    . procédé
	test des annotations
		si annotation bloque, lance sur neighbour_left pour savoir le timeout nécessaire
	chgt d'abord des annotations bloquées sur established
	chgt ensuite des annotations bloquées sur preserved
    . notes
	probleme size_t : t-1, t+1 -> ajout de require 0 < length et length < SIZE_MAX
	
